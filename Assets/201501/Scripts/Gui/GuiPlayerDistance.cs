﻿using UnityEngine;
using System.Collections;

public class GuiPlayerDistance : MonoBehaviour {

	public float			distanceRatio;
	public GameObject		Target;
	
	void Update () {
	
		float pos = Target.transform.position.y/(GameManager.Instance.GameDistance/GameManager.Instance.GameScale);
		transform.localPosition = Vector3.up * (distanceRatio * pos);
	}
}
