﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager> {

	public GameSounds			GameSounds		{ get; set; }
	public int					GameCoins		{ get; set; }
	public float				GameFuel		{ get; set; }
	public float				GameDistance	{ get; set; }
	public float				GameScale		{ get; set; }	
	public int					GameScore		{ get; set; }
	public PlayerController		PlayerController	{ get; set; }
	public GameController		GameController	{ get; set; }
}
