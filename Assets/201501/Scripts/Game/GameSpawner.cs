﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Range {
	public int 	bottom;	
	public int	top;
	public int  left;
	public int  right;	
}

[System.Serializable]
public struct SpawnTotal {
	public int min;
	public int max;

}

public class GameSpawner : MonoBehaviour {

	public GameObject[]			prefab;
	public Range				range;
	public SpawnTotal			total;
	
	// Use this for initialization
	void Start () {
	
		if (total.max == 0) {
			total.max = total.min;
		}
		
		for(int i = 0; i < Random.Range(total.min,total.max); i++) {
			
			float y = Random.Range(range.bottom, range.top);
			float x = Random.Range(range.left,range.right);
			Vector3 pos = new Vector3(x,y,0.0f);
			
			GameObject go = (GameObject)Instantiate(prefab[Random.Range(0,prefab.Length)]);
			go.transform.parent = transform;
			go.isStatic = true;
			go.transform.position = pos + new Vector3(0,0,0.1f);
			
		}	
	}
}
