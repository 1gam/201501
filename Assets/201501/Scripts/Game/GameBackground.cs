﻿using UnityEngine;
using System.Collections;

public class GameBackground : MonoBehaviour {

	public GameObject			Target;
	public float				speed;
	public int					stage;
	public int					distance;
	
	private Vector3				start;

	void OnEnable() {
		start = transform.position;
	}
	
	void LateUpdate () {
	
		Vector3 newPosition;
		newPosition =  start +  (new Vector3(0,1,0) * (Target.transform.position.y/distance * speed));
		newPosition +=  (new Vector3(1,0,0) * (Target.transform.position.x/40 * speed));
		
		transform.position = newPosition;
	}
}
