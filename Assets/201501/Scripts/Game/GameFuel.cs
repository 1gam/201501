﻿using UnityEngine;
using System.Collections;

public class GameFuel : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player") {
			GameManager.Instance.GameFuel += 1;
			GameManager.Instance.GameScore += 50;
			GameManager.Instance.GameSounds.PlayPickup();
			Destroy(gameObject);
		}
	}
}
